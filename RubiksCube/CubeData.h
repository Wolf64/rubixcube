#pragma once
#include <glm/glm.hpp>
#include <vector>

enum class TurnAxis
{
	X = -1,
	Y = 0, 
	Z = 1
};

enum class TurnDirection
{
	Clockwise = -1,
	CounterClockwise = 1
};

enum class Disc
{
	TopRight = -1,
	Middle = 0,
	BottomLeft = 1
};

class CubeData
{
public:
	CubeData();
	CubeData(bool random);
	void Reset();
	void Randomize();
	void TurnDisc(TurnAxis axis, TurnDirection direction, Disc disc, bool noSound = false);

	/**************
	* Rendering
	***************/
	//1st column: working index (0 actual, 1 new)
	//2nd-4th column: x, y, z coords
	glm::mat4 m_rotations[2][3][3][3];
	float m_cubieOffset;
};

