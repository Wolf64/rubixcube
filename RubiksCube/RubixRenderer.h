#pragma once

#include "GameInterface.h"
#include "CubieRenderer.h"
#include "Input.h"
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/ext/quaternion_float.hpp>
#include "CubeData.h"

class RubixRenderer : public GameInterface
{
public:
	virtual void Init(GLFWwindow* window);
	virtual void Render(float fAspectRatio);
	virtual void ClearResources();
	virtual void Update(double fDeltaTime);

private:
	CubeData m_data;

	//cubie rending
	CubieRenderer m_cubieRenderer;
	float m_cubieOffset = 0.0f;
	const float m_cubieSpacing = 0.025f;
	glm::quat m_orientationQuaternion = glm::identity<glm::quat>(); 
	glm::vec3 m_cubePos;
	glm::mat4 m_viewProject;

	//GL stuff
	GLuint m_ShaderProgram;
	GLuint m_vertexBufferObject;
	GLuint m_arrayBufferObject;
	GLint m_transformLocation;

	//input
	Input m_input;
	float m_rotationSpeed = 90.0f;

	//inital rotation in euler angles
	glm::vec3 m_initalCubeRotation = glm::vec3(glm::radians(30.0f), glm::radians(135.0f), glm::radians(25.0f));

	//member functions
	TurnAxis GetNearestAxis(TurnAxis axis, int &outDisc);
	void HandleTurns();
	void HandleCubeRotation(float fDeltaTime);
};

