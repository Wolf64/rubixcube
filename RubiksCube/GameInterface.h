#pragma once

struct GLFWwindow;
class GameInterface
{
public:
	virtual void Init() {};	//{} (empty), can use but doesn't need to, make = 0 to force implementation
	virtual void Init(GLFWwindow* window) { Init(); }
	virtual void Update(double fDeltaTime) {};
	virtual void Render(float fAspectRation) {};
	virtual void ClearResources() {};

};

