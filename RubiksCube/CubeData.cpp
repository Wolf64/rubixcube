#include "CubeData.h"
#include <glm/ext.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include "Sound.h"

CubeData::CubeData()
{

}

CubeData::CubeData(bool random = false)
{
	m_cubieOffset = 0.1f;
	Reset();
	Sound::PlayChord(false);
	
	if (random)
		Randomize();
}

void CubeData::Reset()
{
	for (int x = 0; x < 3; ++x)
		for (int y = 0; y < 3; ++y)
			for (int z = 0; z < 3; ++z)
			{
				m_rotations[0][x][y][z] = glm::toMat4(glm::quat(glm::vec3(0, 0, 0)));
				m_rotations[1][x][y][z] = glm::toMat4(glm::quat(glm::vec3(0, 0, 0)));
			}

	Sound::PlayChord(true);
}

void CubeData::Randomize()
{
	std::random_device rnd;
	std::default_random_engine random(rnd());
	std::uniform_int_distribution<int> uniform_count(5, 15);
	std::uniform_int_distribution<int> uniform_param(-1, 1);
	std::uniform_int_distribution<int> uniform_bin(0, 1);
	int iRandomCount = uniform_count(random);

	for (int i = 0; i < iRandomCount; ++i)
	{
		int iTurnDir = uniform_bin(random);
		if (!iTurnDir)
			iTurnDir = -1;

		int iTurnAxis = uniform_param(random);
		int iDisc = uniform_param(random);
		TurnDisc((TurnAxis)iTurnAxis, (TurnDirection)iTurnDir, (Disc)iDisc, true);
	}

	Sound::PlayChord(false);
}

void CubeData::TurnDisc(TurnAxis axis, TurnDirection direction, Disc disc, bool noSound)
{
	int iDisc = (int)disc + 1;
	//rotate top layer y
	if (axis == TurnAxis::Y)
	{
		glm::vec3 EulerAngles(0, glm::radians(90.0f) * (int)direction, 0);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		for(int x = 0; x < 3; ++x)
			for (int z = 0; z < 3; ++z)
				m_rotations[1][x][iDisc][z] = RotationMatrix * m_rotations[0][x][iDisc][z];

		for (int x = 0; x < 3; ++x)
			for (int z = 0; z < 3; ++z)
			{
				glm::vec4 pos = glm::vec4(x-1, iDisc-1, z-1, 1);
				glm::vec4 result = RotationMatrix * pos;
				int xNew = (int)roundf(result.x) + 1;
				int zNew = (int)roundf(result.z) + 1;
				m_rotations[0][xNew][iDisc][zNew] = m_rotations[1][x][iDisc][z];
			}
	}

	//rotate right layer x
	if (axis == TurnAxis::X)
	{
		glm::vec3 EulerAngles(glm::radians(90.0f) * (int)direction, 0, 0);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		for (int y = 0; y < 3; ++y)
			for (int z = 0; z < 3; ++z)
				m_rotations[1][iDisc][y][z] = RotationMatrix * m_rotations[0][iDisc][y][z];

		for (int y = 0; y < 3; ++y)
			for (int z = 0; z < 3; ++z)
			{
				glm::vec4 pos = glm::vec4(iDisc - 1, y - 1, z - 1, 1);
				glm::vec4 result = RotationMatrix * pos;
				int yNew = (int)roundf(result.y) + 1;
				int zNew = (int)roundf(result.z) + 1;
				m_rotations[0][iDisc][yNew][zNew] = m_rotations[1][iDisc][y][z];
			}
	}

	//rotate front layer z
	if (axis == TurnAxis::Z)
	{
		glm::vec3 EulerAngles(0, 0, glm::radians(90.0f) * (int)direction);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		for (int x = 0; x < 3; ++x)
			for (int y = 0; y < 3; ++y)
			m_rotations[1][x][y][iDisc] = RotationMatrix * m_rotations[0][x][y][iDisc];

		for (int x = 0; x < 3; ++x)
			for (int y = 0; y < 3; ++y)
			{
				glm::vec4 pos = glm::vec4(x - 1, y - 1, iDisc - 1, 1);
				glm::vec4 result = RotationMatrix * pos;
				int xNew = (int)roundf(result.x) + 1;
				int yNew = (int)roundf(result.y) + 1;
				m_rotations[0][xNew][yNew][iDisc] = m_rotations[1][x][y][iDisc];
			}
	}

	if(!noSound)
		Sound::PlayRandomClick();
}