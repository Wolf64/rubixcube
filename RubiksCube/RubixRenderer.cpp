#include "RubixRenderer.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include <Windows.h>

void RubixRenderer::Init(GLFWwindow* window)
{
	m_input.SetWindow(window);
	//camera controls
	m_input.ObserveKey(GLFW_KEY_SPACE);
	m_input.ObserveKey(GLFW_KEY_RIGHT);
	m_input.ObserveKey(GLFW_KEY_LEFT);
	m_input.ObserveKey(GLFW_KEY_UP);
	m_input.ObserveKey(GLFW_KEY_DOWN);
	m_input.ObserveKey(GLFW_KEY_RIGHT_SHIFT);
	m_input.ObserveKey(GLFW_KEY_LEFT_SHIFT);
	
	//cube controls
	m_input.ObserveKey(GLFW_KEY_RIGHT_CONTROL);
	m_input.ObserveKey(GLFW_KEY_LEFT_CONTROL);
	m_input.ObserveKey(GLFW_KEY_KP_0);
	m_input.ObserveKey(GLFW_KEY_KP_1);
	m_input.ObserveKey(GLFW_KEY_KP_2);
	m_input.ObserveKey(GLFW_KEY_KP_3);
	m_input.ObserveKey(GLFW_KEY_KP_4);
	m_input.ObserveKey(GLFW_KEY_KP_6);
	m_input.ObserveKey(GLFW_KEY_KP_7);
	m_input.ObserveKey(GLFW_KEY_KP_8);
	m_input.ObserveKey(GLFW_KEY_KP_9);

	//reset
	m_input.ObserveKey(GLFW_KEY_KP_5);

	m_cubePos = glm::vec3(0.0f);
	m_viewProject = glm::mat4(0.0f);
	m_cubieRenderer.Init();

	m_orientationQuaternion = glm::quat(m_initalCubeRotation);

	//generate a cube with random positions
	m_data = CubeData(true);

	m_cubieOffset = m_cubieRenderer.GetCubieExtension() + m_cubieSpacing;
}

void RubixRenderer::Render(float fAspectRatio)
{
	float nearPlane = 0.1f;
	float farPlane = 100.0f;

	m_viewProject = glm::perspective(glm::radians(45.0f), fAspectRatio, nearPlane, farPlane) *
		glm::lookAt(glm::vec3(0.0f, 0.0f, -9.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
		glm::mat4_cast(m_orientationQuaternion);

	for(int i = 0; i < 3; ++i)
		for(int j = 0; j < 3; ++j)
			for (int k = 0; k < 3; ++k)
			{
				glm::vec3 pos = glm::vec3((i - 1) * m_cubieOffset, (j - 1) * m_cubieOffset, (k - 1) * m_cubieOffset);
				glm::mat4 compound = glm::translate(m_viewProject, pos);
				
				//apply rotations from data
				compound *= m_data.m_rotations[0][i][j][k];

				glm::mat4 finalMat = glm::translate(compound, m_cubePos);
				m_cubieRenderer.Render(finalMat);
			}
}

void RubixRenderer::ClearResources()
{
	m_cubieRenderer.ClearResources();
}

void RubixRenderer::Update(double fDeltaTime)
{
	//rotation after new position doesn't work
	//only works if mouse is held down??
	//rotation center is not in the compound cube anymore but outside
	if (m_input.IsRightMouseDown())
	{
		//glm::vec3 pos, direction;
		//m_cubePos = pos + 9.0f * direction;
	}

	if (m_input.IsLeftMouseDown())
	{
		glm::vec3 pos, direction;
		m_input.GetPickingRay(m_viewProject, pos, direction);
	}

	m_input.Update();

	HandleTurns();
	HandleCubeRotation((float)fDeltaTime);

	if (m_input.WasKeyPressed(GLFW_KEY_SPACE))
		m_orientationQuaternion = glm::quat(m_initalCubeRotation);
	if (m_input.WasKeyPressed(GLFW_KEY_KP_5))
		m_data.Randomize();
	if (m_input.WasKeyPressed(GLFW_KEY_KP_0))
		m_data.Reset();
}

void RubixRenderer::HandleTurns()
{
	TurnAxis axis;
	int iDisc = 0;

	//left/right controls
	if (m_input.IsKeyDown(GLFW_KEY_LEFT_CONTROL) || m_input.IsKeyDown(GLFW_KEY_RIGHT_CONTROL))
	{
		axis = GetNearestAxis(TurnAxis::X, iDisc);
		if (m_input.WasKeyPressed(GLFW_KEY_KP_9))
			m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::TopRight * iDisc));
		else if (m_input.WasKeyPressed(GLFW_KEY_KP_8))
			m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::Middle * iDisc));
		else if (m_input.WasKeyPressed(GLFW_KEY_KP_7))
			m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::BottomLeft * iDisc));
		else if (m_input.WasKeyPressed(GLFW_KEY_KP_3))
			m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::TopRight * iDisc));
		else if (m_input.WasKeyPressed(GLFW_KEY_KP_2))
			m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::Middle * iDisc));
		else if (m_input.WasKeyPressed(GLFW_KEY_KP_1))
			m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::BottomLeft * iDisc));

		return;
	}
	
	//up/down controls
	axis = GetNearestAxis(TurnAxis::Y, iDisc);
	if (m_input.WasKeyPressed(GLFW_KEY_KP_9))
		m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::TopRight * iDisc));
	else if (m_input.WasKeyPressed(GLFW_KEY_KP_6))
		m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::Middle * iDisc));
	else if (m_input.WasKeyPressed(GLFW_KEY_KP_3))
		m_data.TurnDisc(axis, TurnDirection::CounterClockwise, Disc((int)Disc::BottomLeft * iDisc));
	else if (m_input.WasKeyPressed(GLFW_KEY_KP_7))
		m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::TopRight * iDisc));
	else if (m_input.WasKeyPressed(GLFW_KEY_KP_4))
		m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::Middle * iDisc));
	else if (m_input.WasKeyPressed(GLFW_KEY_KP_1))
		m_data.TurnDisc(axis, TurnDirection::Clockwise, Disc((int)Disc::BottomLeft * iDisc));
}

void RubixRenderer::HandleCubeRotation(float fDeltaTime)
{
	float rotDir = 1;	//used for face rotation, pressing shift inverts that
	float rotSpeed = m_rotationSpeed;

	//mod rotation speed by factor 2
	if (m_input.IsKeyDown(GLFW_KEY_LEFT_SHIFT) || m_input.IsKeyDown(GLFW_KEY_RIGHT_SHIFT))
	{
		rotSpeed *= 2;
		rotDir = -1;
	}

	float xVel = 0.0f;
	if (m_input.IsKeyDown(GLFW_KEY_UP))
		xVel = glm::radians(rotSpeed);
	if (m_input.IsKeyDown(GLFW_KEY_DOWN))
		xVel = glm::radians(-rotSpeed);

	float yVel = 0.0f;
	if (m_input.IsKeyDown(GLFW_KEY_RIGHT))
		yVel = glm::radians(rotSpeed);
	if (m_input.IsKeyDown(GLFW_KEY_LEFT))
		yVel = glm::radians(-rotSpeed);


	glm::quat velocityQuaternion = glm::quat(0.0f, glm::vec3(xVel, yVel, 0.0f));

	m_orientationQuaternion += 0.5f * fDeltaTime * velocityQuaternion * m_orientationQuaternion;
	m_orientationQuaternion = normalize(m_orientationQuaternion);
}

TurnAxis RubixRenderer::GetNearestAxis(TurnAxis axis, int &outDisc)
{
	//get column vectors representing axes
	glm::vec4 xAxis = m_viewProject[0];
	glm::vec4 yAxis = m_viewProject[1];
	glm::vec4 zAxis = m_viewProject[2];

	TurnAxis solution = TurnAxis::X;

	switch (axis)
	{
	case TurnAxis::X:
		if (fabs(xAxis.x) > fabs(yAxis.x) && fabs(xAxis.x) > fabs(zAxis.x))
		{
			solution = TurnAxis::X;
			outDisc = xAxis.x < 0 ? 1 : -1;
		}
		else if (fabs(yAxis.x) > fabs(zAxis.x) && fabs(yAxis.x) > fabs(xAxis.x))
		{
			solution = TurnAxis::Y;
			outDisc = yAxis.x < 0 ? 1 : -1;
		}
		else
		{
			solution = TurnAxis::Z;
			outDisc = zAxis.x < 0 ? 1 : -1;
		}
		break;
	case TurnAxis::Y:
		if (fabs(xAxis.y) > fabs(yAxis.y) && fabs(xAxis.y) > fabs(zAxis.y))
		{
			solution = TurnAxis::X;
			outDisc = xAxis.y < 0 ? 1 : -1;
		}
		else if (fabs(yAxis.y) > fabs(zAxis.y) && fabs(yAxis.y) > fabs(xAxis.y))
		{
			solution = TurnAxis::Y;
			outDisc = yAxis.y < 0 ? 1 : -1;
		}
		else
		{
			solution = TurnAxis::Z;
			outDisc = zAxis.y < 0 ? 1 : -1;
		}
		break;
	}

	return solution;
}
