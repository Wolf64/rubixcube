#include "GameInterface.h"
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "RubixRenderer.h"

//test classes
RubixRenderer gRubixRenderer;

//actual instance we'll use no matter what it is (e.g. for testing or other)
GameInterface* gCurrentInterface;

GLFWwindow* InitializeSystem()
{
    //basic init stuff
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(1024, 768, "Rubiks Cube", nullptr, nullptr);
    glfwMakeContextCurrent(window); //all render instructions go to this window

    glewExperimental = GL_TRUE; //use opengl function pointers
    glewInit();

    //do any custom init stuff we defined
    gCurrentInterface->Init(window);

    return window;
}

void RunCoreLoop(GLFWwindow* window)
{
    double lastTime = glfwGetTime();
    double timeDiff = 0.0f;

    while (!glfwWindowShouldClose(window))
    {
        //for input
        glfwPollEvents();

        gCurrentInterface->Update(timeDiff);

        //prepare rendering
        int iScreenWidth, iScreenHeight;
        glfwGetFramebufferSize(window, &iScreenWidth, &iScreenHeight);
        float aspect = (float)iScreenWidth / (float)iScreenHeight;

        //wipe viewport
        glViewport(0, 0, iScreenWidth, iScreenHeight);

        //make sure depth test is enable
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //do our rendering stuff
        gCurrentInterface->Render(aspect);

        //to actually see stuff...
        glfwSwapBuffers(window);

        double currentTime = glfwGetTime();
        timeDiff = currentTime - lastTime;
        lastTime = currentTime;
    }
}

void ShutdownSystem()
{
    gCurrentInterface->ClearResources();
    glfwTerminate();
}

int main()
{
    gCurrentInterface = &gRubixRenderer;
    GLFWwindow* window = InitializeSystem();
    RunCoreLoop(window);
    ShutdownSystem();
}

