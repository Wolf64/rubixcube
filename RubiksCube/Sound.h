#pragma once
#include <Windows.h>
#include <random>
#include <string>

class Sound
{
public:
	static void PlayRandomClick();
	static void PlayChord(bool reset, bool random = true);
};

