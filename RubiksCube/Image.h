#pragma once
#include "stb_image.h"
#include <string>

#define STB_IMAGE_IMPLEMENTATION

class Image
{
public:
	static unsigned char* LoadImageRaw(std::string filename);
};

