#version 330

in vec3 vertColor;
out vec4 color;

void main()
{
    float ambientStrength = 0.75;
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    vec3 ambient = ambientStrength * lightColor;
    vec3 result = ambient * vertColor;

	color = vec4(result, 1.0);
}