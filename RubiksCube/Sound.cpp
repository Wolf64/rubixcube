#include "Sound.h"

void Sound::PlayRandomClick()
{
	std::random_device rnd;
	std::default_random_engine random(rnd());
	std::uniform_int_distribution<int> uniform_dist(1, 4);
	int i = uniform_dist(random);

	std::string str("cube_turn_0" + std::to_string(i) + ".wav");
	std::wstring wstr = std::wstring(str.begin(), str.end());

	PlaySound(wstr.c_str(), NULL, SND_FILENAME | SND_ASYNC);
}

void Sound::PlayChord(bool reset, bool random)
{
	int i = 1;
	if (random)
	{
		std::random_device rd;
		std::default_random_engine re(rd());
		std::uniform_int_distribution<int> uniform_dist(1, 5);
		i = uniform_dist(re);
	}

	std::string str = "chord_";
	if (reset)
		str += "reset_";
	str += "0" + std::to_string(i) + ".wav";
	std::wstring wstr = std::wstring(str.begin(), str.end());

	PlaySound(wstr.c_str(), NULL, SND_FILENAME | SND_ASYNC);
}
