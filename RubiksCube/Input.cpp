#include "Input.h"
#include <GLFW/glfw3.h>

void Input::Update()
{
	for (auto i = m_keyMapper.begin(); i != m_keyMapper.end(); ++i)
		i->second.Update();
}

void Input::ObserveKey(int key)
{
	m_keyMapper[key] = KeyboardObserver(m_window, key);
}

bool Input::IsLeftMouseDown()
{
	return glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
}

bool Input::IsRightMouseDown()
{
	return glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS;
}

void Input::GetPickingRay(const glm::mat4& transformMatrix, glm::vec3& startingPoint, glm::vec3& direction)
{
	double xpos, ypos;
	glfwGetCursorPos(m_window, &xpos, &ypos);

	//translate to clip space coords (-1 to 1)
	int screenWidth, screenHeight;
	glfwGetFramebufferSize(m_window, &screenWidth, &screenHeight);

	xpos = (xpos / screenWidth) * 2.0 - 1.0;
	ypos = 1.0 - 2.0 * (ypos / screenHeight);

	//build start and end points
	glm::vec4 nearPoint = glm::vec4((float)xpos, (float)ypos, 0.01f, 1.0f);
	glm::vec4 farPoint = nearPoint;
	farPoint.z = 0.99f;

	//translate to world-/object space
	glm::mat4 inverse = glm::inverse(transformMatrix);
	nearPoint = inverse * nearPoint;
	farPoint = inverse * farPoint;

	//remove perspective, make karthesisch again
	nearPoint /= nearPoint.w;
	farPoint /= farPoint.w;

	startingPoint = nearPoint;
	direction = farPoint - nearPoint;

	direction = glm::normalize(direction);
}

void Input::GetMousePos(double& xpos, double& ypos)
{
	glfwGetCursorPos(m_window, &xpos, &ypos);
}
