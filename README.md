# RubixCube (C) 2020 - 2021 Alexander Marx

--- Projekt zum Modul Spielekonsolenprogrammierung, Prof. Lürig ---

<====================== Controls ======================>

|--- Cube Rotation ---|

Arrow keys: 		rotate whole cube
Shift + Arrow keys: rotate cube faster

|--- Horizontal Layer Controls (camera relative) ---|

Numpad 1: 			rotate bottom layer left
Numpad 3: 			rotate bottom layer right
Numpad 4: 			rotate middle layer left
Numpad 6: 			rotate middle layer right
Numpad 7: 			rotate top layer left
Numpad 9: 			rotate top layer right

|--- Vertical Layer Controls (camera relative) ---|

Ctrl + Numpad 1: 	rotate left layer downwards
Ctrl + Numpad 7: 	rotate left layer upwards
Ctrl + Numpad 2: 	rotate middle layer downwars
Ctrl + Numpad 8: 	rotate middle layerupwards
Ctrl + Numpad 3: 	rotate right layer downwards
Ctrl + Numpad 9: 	rotate right layer upwards

|--- Other ---|

Numpad 0: 			reset/solve cube
Numpad 5: 			randomize cube
